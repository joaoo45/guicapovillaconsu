@extends('master')

@section('content')
    <!-- start banner Area -->
    <section class="banner-area">
        <div class="container">
            <div class="row fullscreen align-items-center justify-content-between">
                <div class="col-lg-6 col-md-6 banner-left">
                    <h6>CONSU 2018 - 2020</h6>
                    <h1>#1 Gui Capovilla</h1>
                    <p>Colocar o slogan aqui</p>
                    <p>
                    </p>
                    <a href="#home-about" class="primary-btn text-uppercase">Ver mais</a>
                </div>
                <div class="col-lg-6 col-md-6 banner-right d-flex align-self-end">
                    <img class="img-fluid principal" src="img/guilherme_png.png" alt="">
                </div>
            </div>
        </div>
    </section>
    <!-- End banner Area -->

    <!-- Start home-about Area -->
    <section id="home-about" class="home-about-area pt-120">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-lg-6 col-md-6 home-about-left">
                    <img class="img-fluid" src="img/about-img.png" alt="">
                </div>
                <div class="col-lg-5 col-md-6 home-about-right">
                    <h6>Sobre mim</h6>
                    <h1 class="text-uppercase">Biografia</h1>
                    <p>
                        Formado em Gestão de Políticas Públicas pela USP e pós-graduado em Gerenciamento de
                        Projetos pela FGV, Guilherme Capovilla é apaixonado pela UNICAMP e deseja fazer uma
                        evolução no sistema participativo da Universidade.
                    </p>
                    <a href="#" class="primary-btn text-uppercase">Ver mais</a>
                </div>
            </div>
        </div>
    </section>
    <!-- End home-about Area -->

    <!-- Start services Area -->
    <section class="services-area section-gap">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="menu-content  col-lg-7">
                    <div class="title text-center">
                        <h1 class="mb-10">Minhas propostas</h1>
                        <p>At about this time of year, some months after New Year’s resolutions have been made and kept, or made and neglected.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="single-services">
                        <span class="lnr lnr-pie-chart"></span>
                        <a href="#"><h4>Web Design</h4></a>
                        <p>
                            “It is not because things are difficult that we do not dare; it is because we do not dare that they are difficult.”
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single-services">
                        <span class="lnr lnr-laptop-phone"></span>
                        <a href="#"><h4>Web Development</h4></a>
                        <p>
                            If you are an entrepreneur, you know that your success cannot depend on the opinions of others. Like the wind, opinions.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single-services">
                        <span class="lnr lnr-camera"></span>
                        <a href="#"><h4>Photography</h4></a>
                        <p>
                            Do you want to be even more successful? Learn to love learning and growth. The more effort you put into improving your skills.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single-services">
                        <span class="lnr lnr-picture"></span>
                        <a href="#"><h4>Clipping Path</h4></a>
                        <p>
                            Hypnosis quit smoking methods maintain caused quite a stir in the medical world over the last two decades. There is a lot of argument.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single-services">
                        <span class="lnr lnr-tablet"></span>
                        <a href="#"><h4>Apps Interface</h4></a>
                        <p>
                            Do you sometimes have the feeling that you’re running into the same obstacles over and over again? Many of my conflicts.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single-services">
                        <span class="lnr lnr-rocket"></span>
                        <a href="#"><h4>Graphic Design</h4></a>
                        <p>
                            You’ve heard the expression, “Just believe it and it will come.” Well, technically, that is true, however, ‘believing’ is not just thinking that.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End services Area -->

    <!-- Start fact Area -->
    <section class="facts-area section-gap" id="facts-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 single-fact">
                    <h1 class="counter">4</h1>
                    <p>Anos de Unicamp</p>
                </div>
                <div class="col-lg-3 col-md-6 single-fact">
                    <h1 class="counter">15</h1>
                    <p>Projetos realizados</p>
                </div>
                <div class="col-lg-3 col-md-6 single-fact">
                    <h1 class="counter">7</h1>
                    <p>Contribuições</p>
                </div>
                <div class="col-lg-3 col-md-6 single-fact">
                    <h1 class="counter">8</h1>
                    <p>XYZ</p>
                </div>
            </div>
        </div>
    </section>
    <!-- end fact Area -->

    <!-- Start testimonial Area -->
    <section class="testimonial-area section-gap">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="menu-content pb-70 col-lg-8">
                    <div class="title text-center">
                        <h1 class="mb-10">Apoiadores</h1>
                        <p>It is very easy to start smoking but it is an uphill task to quit it. Ask any chain smoker or even a person.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="active-testimonial">
                    <div class="single-testimonial item d-flex flex-row">
                        <div class="thumb">
                            <img class="img-fluid" src="img/elements/user1.png" alt="">
                        </div>
                        <div class="desc">
                            <p>
                                Do you want to be even more successful? Learn to love learning and growth. The more effort you put into improving your skills, the bigger the payoff you.
                            </p>
                            <h4>Rubens</h4>
                            <p>PRU</p>
                        </div>
                    </div>
                    <div class="single-testimonial item d-flex flex-row">
                        <div class="thumb">
                            <img class="img-fluid" src="img/elements/user2.png" alt="">
                        </div>
                        <div class="desc">
                            <p>
                                A purpose is the eternal condition for success. Every former smoker can tell you just how hard it is to stop smoking cigarettes. However.
                            </p>
                            <h4>Carolyn Craig</h4>
                            <p>CEO at Facebook</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End testimonial Area -->

    <!-- Start recent-blog Area -->
    <section class="recent-blog-area section-gap">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 pb-30 header-text">
                    <h1>Latest posts from our blog</h1>
                    <p>
                        You may be a skillful, effective employer but if you don’t trust your personnel and the opposite, then the chances of improving and expanding the business
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="single-recent-blog col-lg-4 col-md-4">
                    <div class="thumb">
                        <img class="f-img img-fluid mx-auto" src="img/b1.jpg" alt="">
                    </div>
                    <div class="bottom d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                            <img class="img-fluid" src="img/user.png" alt="">
                            <a href="#"><span>Mark Wiens</span></a>
                        </div>
                        <div class="meta">
                            13th Dec
                            <span class="lnr lnr-heart"></span> 15
                            <span class="lnr lnr-bubble"></span> 04
                        </div>
                    </div>
                    <a href="#">
                        <h4>Break Through Self Doubt
                            And Fear</h4>
                    </a>
                    <p>
                        Dream interpretation has many forms; it can be done be done for the sake of fun, hobby or can be taken up as a serious career.
                    </p>
                </div>
                <div class="single-recent-blog col-lg-4 col-md-4">
                    <div class="thumb">
                        <img class="f-img img-fluid mx-auto" src="img/b2.jpg" alt="">
                    </div>
                    <div class="bottom d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                            <img class="img-fluid" src="img/user.png" alt="">
                            <a href="#"><span>Mark Wiens</span></a>
                        </div>
                        <div class="meta">
                            13th Dec
                            <span class="lnr lnr-heart"></span> 15
                            <span class="lnr lnr-bubble"></span> 04
                        </div>
                    </div>
                    <a href="#">
                        <h4>Portable Fashion for
                            young women</h4>
                    </a>
                    <p>
                        You may be a skillful, effective employer but if you don’t trust your personnel and the opposite, then the chances of improving.
                    </p>
                </div>
                <div class="single-recent-blog col-lg-4 col-md-4">
                    <div class="thumb">
                        <img class="f-img img-fluid mx-auto" src="img/b3.jpg" alt="">
                    </div>
                    <div class="bottom d-flex justify-content-between align-items-center flex-wrap">
                        <div>
                            <img class="img-fluid" src="img/user.png" alt="">
                            <a href="#"><span>Mark Wiens</span></a>
                        </div>
                        <div class="meta">
                            13th Dec
                            <span class="lnr lnr-heart"></span> 15
                            <span class="lnr lnr-bubble"></span> 04
                        </div>
                    </div>
                    <a href="#">
                        <h4>Do Dreams Serve As
                            A Premonition</h4>
                    </a>
                    <p>
                        So many of us are demotivated to achieve anything. Such people are not enthusiastic about anything. They don’t want to work involved.
                    </p>
                </div>


            </div>
        </div>
    </section>
    <!-- end recent-blog Area -->

    <section class="recent-blog-area section-gap">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 pb-30 header-text">
                    <h1>Contato</h1>
                    <p>
                        Ficou alguma dúvida? Deseja fazer sugestão? Entre em contato comigo!
                    </p>
                </div>
                <h2 class="col-md-8 pb-30 header-text text-center">
                    <a href="mailto:guigc@unicamp.br">guigc@unicamp.br</a>
                </h2>
            </div>
        </div>
    </section>
@endsection